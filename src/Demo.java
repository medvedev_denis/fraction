import java.util.Scanner;

public class Demo {
    static Scanner scanner = new Scanner(System.in);
    static String numExpression;
    public static void main(String[] args) {
        System.out.println("Введите выражение");
        numExpression=scanner.nextLine();
        FracString fracString = new FracString(numExpression);
        Fraction[] fractions = fracString.fracString();
        Fraction frac1 = fractions[0];
        Fraction frac2 = fractions[1];
        resultOperation(fracString, frac1, frac2);
    }

    public static void resultOperation(FracString fracString, Fraction frac1, Fraction frac2) {
        char lineChar = fracString.getLineChar();
        char[] possibleSigns = {'+', '-', '*', '/'};
        Fraction result = new Fraction();
        for (int i = 0;i < 4 ; i++) {
            int flag;
            if (lineChar == possibleSigns[i]) {
                flag = 1;
                i = 4;
            } else {
                flag = 0;
            }
            switch (flag) {
                case (1):
                    switch (lineChar) {
                        case ('+'):
                            result = Rational.addition(frac1, frac2);
                            break;
                        case ('-'):
                            result = Rational.substraction(frac1, frac2);
                            break;
                        case ('*'):
                            result = Rational.multiplication(frac1, frac2);
                            break;
                        case ('/'):
                            result = Rational.division(frac1, frac2);
                            break;
                    }
                default:
            }
        }
        if (result.getNumerator() == result.getDenominator()) {
            result.setNumerator(1);
            result.setDenominator(1);
        } else {
            if ((result.getNumerator() % result.getDenominator()) == 0){
                result.setNumerator(result.getNumerator() / result.getDenominator());
                result.setDenominator(1);
            } else {
                if ((result.getDenominator() % result.getNumerator()) == 0){
                    result.setDenominator(result.getDenominator() / result.getNumerator());
                    result.setNumerator(1);
                }
            }
        }
        System.out.println(result);
    }
}
