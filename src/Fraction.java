public class Fraction {
    private int numerator;//числитель
    private int denominator;//знаменатель

    public Fraction() {
        this(1,1);
    }

    public Fraction(int numerator) {
        this(numerator,1);
    }

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    @Override
    public String toString() {
        return numerator +
                "/" + denominator;
    }
}
