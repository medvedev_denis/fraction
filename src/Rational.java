public class Rational {


    //Метод сложение
    public static Fraction addition(Fraction frac1, Fraction frac2) {
        Fraction result;
        if (frac1.getDenominator() == frac2.getDenominator()) {
            result = new Fraction(frac1.getNumerator() + frac2.getNumerator(), frac1.getDenominator());
        } else {
            if ((frac1.getDenominator() % frac2.getDenominator()) == 0){
                result = new Fraction(frac1.getNumerator() + frac2.getNumerator() * (frac1.getDenominator()/frac2.getDenominator()) , frac1.getDenominator());
            } else {
                if ((frac2.getDenominator() % frac1.getDenominator()) == 0) {
                    result = new Fraction(frac2.getNumerator() + frac1.getNumerator() * (frac2.getDenominator()/frac1.getDenominator()) , frac2.getDenominator());
                } else {
                    result = new Fraction(frac1.getNumerator() * frac2.getDenominator() + frac2.getNumerator() * frac1.getDenominator(), frac1.getDenominator() * frac2.getDenominator());
                }
            }
        }
        return result;
    }


    //Метод вычитание
    public static Fraction substraction(Fraction frac1, Fraction frac2) {
        Fraction result;
        if (frac1.getDenominator() == frac2.getDenominator()) {
            result = new Fraction(frac1.getNumerator() - frac2.getNumerator(), frac1.getDenominator());
        } else {
            if ((frac1.getDenominator() % frac2.getDenominator()) == 0){
                result = new Fraction(frac1.getNumerator() - frac2.getNumerator() * (frac1.getDenominator()/frac2.getDenominator()) , frac1.getDenominator());
            } else {
                if ((frac2.getDenominator() % frac1.getDenominator()) == 0) {
                    result = new Fraction(frac1.getNumerator() * (frac2.getDenominator()/frac1.getDenominator()) - frac2.getNumerator() , frac2.getDenominator());
                } else {
                    result = new Fraction(frac1.getNumerator() * frac2.getDenominator() - frac2.getNumerator() * frac1.getDenominator() , frac1.getDenominator() * frac2.getDenominator());
                }
            }
        }
        return result;
    }


    //Метод умножение
    public static Fraction multiplication(Fraction frac1, Fraction frac2) {
        Fraction result = new Fraction(frac1.getNumerator() * frac2.getNumerator(), frac1.getDenominator() * frac2.getDenominator());
        return result;
    }


    //Метод деление
    public static Fraction division(Fraction frac1, Fraction frac2) {
        Fraction result = new Fraction(frac1.getNumerator() * frac2.getDenominator(), frac1.getDenominator() * frac2.getNumerator());
        return result;
    }


}
