public class FracString {

    private String numExpression;
    private char lineChar;

    public FracString(String numExpression) {
        this.numExpression = numExpression;
    }

    public Fraction[] fracString(){
        String[] lineComponents = numExpression.split(" ");
        String[] fracComponents = {lineComponents[0], lineComponents[2]};
        String[][] lineComponentNums = new String[2][2];
        for (int i = 0; i < fracComponents.length; i++) {
            if(fracComponents[i].matches(".*/.*")) {
                lineComponentNums[i] = fracComponents[i].split("/");
            } else {
                lineComponentNums[i][0] = fracComponents[i];
                lineComponentNums[i][1] = "1";
            }
        }
        int[] intNums = new int[4];
        int elemMas = 0;
        for (int i = 0; i < lineComponentNums.length; i++) {
            for (int j = 0; j < lineComponentNums[i].length; j++) {
                intNums[elemMas] = Integer.valueOf(lineComponentNums[i][j]);
                elemMas++;
            }

        }
        lineChar = lineComponents[1].charAt(0);
        Fraction fraction1 = new Fraction(intNums[0], intNums[1]);
        Fraction fraction2 = new Fraction(intNums[2], intNums[3]);
        Fraction[] fractions = {fraction1, fraction2};
        return fractions;
    }

    public char getLineChar() {
        return lineChar;
    }
}